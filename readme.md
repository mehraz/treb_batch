
#Description
This batch app reads the PDF files which are collected from http://www.abonysteinberg.com/monthly-market-watch.html.
It then saves it in a mongo db for later data processing.

#Setup
install as a regular POM project.

#Requirments:
Mongo 3.2
JRE 1.8
