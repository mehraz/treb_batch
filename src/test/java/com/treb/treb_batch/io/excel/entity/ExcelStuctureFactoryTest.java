package com.treb.treb_batch.io.excel.entity;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.treb.treb_batch.config.TrebConfig;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:contexts/orderprocess_context.xml" })
@ActiveProfiles(profiles = "test")
public class ExcelStuctureFactoryTest {
	
	@Mock
	TrebConfig config;
	
	@Mock
	File templateDirectory;
	
	@InjectMocks
	ExcelStuctureFactory strcutreFactory = new ExcelStuctureFactory();
	

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(config.getJsonTemplateDirectory()).thenReturn("excel_templates");
		String path = new File(getClass().getClassLoader().getResource(config.getJsonTemplateDirectory()).getFile()).getPath();
		Mockito.when(templateDirectory.getPath()).thenReturn(path);
	}
	
	@Test
	public void testGetWithNull() {
		ExcelStrcutureImpl structure = strcutreFactory.getByName(null);
		assertNull(structure);
		//to-do
		//fail("Not yet implemented");
	}
	
	@Test
	public void testGetWithNotExistFile() {
		String name = "aa";
		ExcelStrcutureImpl structure = strcutreFactory.getByName(name);
		
		assertNull(structure);
		assertEquals(false, strcutreFactory.isStructureRegistered(name));
	}

}
