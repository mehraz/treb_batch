package com.treb.treb_batch.io.map;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.TestCase;

import com.treb.treb_batch.domain.HouseTypePrice;
import com.treb.treb_batch.domain.utils.HouseTypeUtils;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:contexts/orderprocess_context.xml" })
@ActiveProfiles(profiles = "test")
public class HouseTypePriceMapTest extends TestCase {
	
	@Mock
	HouseTypeUtils houseTypeUtils;

	@InjectMocks
	HouseTypePriceMap mapper = new HouseTypePriceMap();
	
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void mapOne_testEmptyData() {
		HouseTypePrice converted = new HouseTypePrice();
		converted = mapper.mapOne(null,converted, 0, 0);
		assertEquals(converted, null);
		
		ArrayList<ArrayList<String>> data = new ArrayList<>();
		//
		converted = mapper.mapOne(data, converted, 0, 0);
		assertEquals(converted, null);
	}
	
	@Test
	public void mapOne_testOutOfBoundIndexes() {
		ArrayList<ArrayList<String>> data = get1RowData();
		HouseTypePriceMap mapper = new HouseTypePriceMap();
		//zero values
		HouseTypePrice converted = new HouseTypePrice();
		converted = mapper.mapOne(data,converted, 0, 0);
		assertEquals(converted, null);
		//neg values
		converted = mapper.mapOne(data, converted, -1, -4);
		assertEquals(converted, null);
		//
		converted = mapper.mapOne(data, converted, 1, 14);
		assertEquals(converted, null);
		converted = mapper.mapOne(data, converted, 3, 1);
		assertEquals(converted, null);
	}
	
	
	
	@Test
	public void mapeOne_testCorrectInput() {
		ArrayList<ArrayList<String>> data = get1RowData();
		String houseTypeName = "Detached";
		String houseTypeUni = "detached";
		Mockito.when(houseTypeUtils.getHouseTypeUniByName(houseTypeName)).thenReturn(houseTypeUni);
		
		HouseTypePrice converted = new HouseTypePrice();
		converted = mapper.mapOne(data, converted, 1, 1);
		assertEquals(converted.getDate(), "20161030");
		assertEquals(converted.getHouseTypeUniName(), houseTypeUni);
		assertEquals(converted.getPriceMin(), Integer.valueOf(0));
		assertEquals(converted.getPriceMax(), Integer.valueOf(99999));
		assertEquals(converted.getTotalSale(),Integer.valueOf(0));
		//Assert
	}
	
	@Test
	public void mapOne_testWithOnlyOneRange() {
		ArrayList<ArrayList<String>> data = get1RowData();
		String houseTypeName = "Detached";
		String houseTypeUni = "detached";
		Mockito.when(houseTypeUtils.getHouseTypeUniByName(houseTypeName)).thenReturn(houseTypeUni);
		
		HouseTypePrice converted = new HouseTypePrice();
		converted = mapper.mapOne(data, converted, 2, 1);
		assertEquals(converted.getDate(), "20161030");
		assertEquals(converted.getHouseTypeUniName(), houseTypeUni);
		assertEquals(converted.getPriceMin(), Integer.valueOf(99999));
		assertEquals(converted.getPriceMax(), Integer.valueOf(Integer.MAX_VALUE));
		assertEquals(converted.getTotalSale(),Integer.valueOf(0));
		//Assert
	}
	
	@Test
	public void mapAll_testWithNull(){
		
		ArrayList<HouseTypePrice> result = mapper.mapAll(null);
		assertEquals(result, null);
	
		
	}
	
	@Test
	public void mapAll_testWithEmptyData(){
		String houseTypeName = "Detached";
		String houseTypeUni = "detached";
		Mockito.when(houseTypeUtils.getHouseTypeUniByName(houseTypeName)).thenReturn(houseTypeUni);
		ArrayList<HouseTypePrice> result = mapper.mapAll(new ArrayList<ArrayList<String>>() );
		assertNotNull(result);
		assertEquals(result.size(), 0);

	}
	
	@Test
	public void mapAll_testWithActualData(){
		ArrayList<ArrayList<String>> data = get1RowData();
		String houseTypeName = "Detached";
		String houseTypeUni = "detached";
		Mockito.when(houseTypeUtils.getHouseTypeUniByName(houseTypeName)).thenReturn(houseTypeUni);
		ArrayList<HouseTypePrice> result = mapper.mapAll(data );
		assertNotNull(result);
		assertEquals(result.size(), 18);
		
	}
	
	private ArrayList<ArrayList<String>> get1RowData(){
		
		ArrayList<String> row1 = new ArrayList<String>();
		row1.addAll(Arrays.asList(new String[]{"20161030", "Detached", "Semi-Detached", "Att/Row/Twnhouse", "Condo Townhouse", "Condo Apt", "Link", "Co-op Apt", "Det Condo", "Co-ownership Apt"}));
		ArrayList<String> row2 = new ArrayList<String>();
		row2.addAll(Arrays.asList(new String[]{"$0 to $99,999", "0.0", "0.0", "0.0", "0.0", "9.0", "0.0", "0.0", "0.0", "0.0"}));
		ArrayList<String> row3 = new ArrayList<String>();
		row3.addAll(Arrays.asList(new String[]{"$99,999", "0.0", "0.0", "0.0", "0.0", "9.0", "0.0", "0.0", "0.0", "0.0"}));
		ArrayList<ArrayList<String>> data = new ArrayList<>();
		data.add(row1);
		data.add(row2);
		data.add(row3);
		return data;
	}

}
