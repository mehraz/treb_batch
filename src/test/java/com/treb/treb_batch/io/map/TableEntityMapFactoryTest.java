package com.treb.treb_batch.io.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:contexts/orderprocess_context.xml" })
@ActiveProfiles(profiles = "test")
public class TableEntityMapFactoryTest {
	
	@Mock
	HouseTypePriceMap hseTPriceMap;
	
	@InjectMocks
	TableEntityMapFactory mapFactory = new TableEntityMapFactory();
	

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetMapperWithEmptyString() {
		TableToEntityMap<?> mapper = mapFactory.getMapper("");
		assertNull(mapper);
	}

	@Test
	public void testGetMapperWithNull() {
		TableToEntityMap<?> mapper = mapFactory.getMapper(null);
		assertNull(mapper);
	}

	@Test
	public void testGetMapperWithNotRegisteredValue() {
		TableToEntityMap<?> mapper = mapFactory.getMapper("aaa");
		assertNull(mapper);
	}
	
	
	@Test
	public void testGetMapperWithRegisteredValue() {
		TableToEntityMap<?> mapper = mapFactory.getMapper("housetype_price");
		assertNotNull(mapper);
		assertTrue(mapper instanceof HouseTypePriceMap);
	}
}
