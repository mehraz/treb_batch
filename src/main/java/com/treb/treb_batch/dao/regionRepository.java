package com.treb.treb_batch.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.treb.treb_batch.domain.Region;

public interface regionRepository extends CrudRepository<Region, String> {
	
	@Query("{'name' : ?0}")
    public Iterable<Region> searchByName(String typeName);
	
	@Query("{'parent' : ?0}")
    public Iterable<Region> searchByParent(String typeName);
}
