package com.treb.treb_batch.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.treb.treb_batch.domain.HouseTypePrice;

public interface HouseTypePriceRepository extends CrudRepository<HouseTypePrice, String> {
	
	@Query("{'house_type' : ?0}")
    public Iterable<HouseTypePrice> searchByName(String typeName);
	
	@Query("{'house_type' : ?0, 'date' : ?1, 'price_min' : ?2, 'price_max' : ?3}")
    public Iterable<HouseTypePrice> searchByAllProps(String houseType, String date, String minPrice, String maxPrice);
}
