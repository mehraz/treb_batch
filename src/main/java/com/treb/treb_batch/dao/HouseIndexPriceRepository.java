package com.treb.treb_batch.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.treb.treb_batch.domain.HouseIndexPrice;

public interface HouseIndexPriceRepository extends CrudRepository<HouseIndexPrice, String> {
	
	
}
