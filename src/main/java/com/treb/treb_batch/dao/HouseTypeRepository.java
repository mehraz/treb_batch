package com.treb.treb_batch.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.treb.treb_batch.domain.HouseType;


public interface HouseTypeRepository extends CrudRepository<HouseType, String>{
			@Query("{'name' : ?0}")
	        public Iterable<HouseType> searchByName(String typeName);

}
