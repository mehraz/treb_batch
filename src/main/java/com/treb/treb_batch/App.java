package com.treb.treb_batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */

public class App {

	private static final Logger logger = LoggerFactory.getLogger(App.class);
	public static void main(String[] args) {
		String[] configs = { "contexts/process_config.xml" };
		ApplicationContext context = new ClassPathXmlApplicationContext(configs);
		logger.info("context loaded. Number of Beans:" + context.getBeanDefinitionCount());
		context.getApplicationName();
	}
}
