package com.treb.treb_batch.domain;

public class Transaction {
	int numberOfSales;
	long totalDollarValume;
	long avgPrice;
	long medianPrice;
	int newListing;
	double snlr;
	int activeListings;
	double mosInv;
	double avgSP_LP;
	double avgDOM;	
	
	
	public int getNumberOfSales() {
		return numberOfSales;
	}
	public void setNumberOfSales(int numberOfSales) {
		this.numberOfSales = numberOfSales;
	}
	public void setNumberOfSales(String numberOfSales) {
		this.numberOfSales = ((Double) Double.parseDouble(numberOfSales)).intValue();
	}
	
	
	public long getTotalDollarValume() {
		return totalDollarValume;
	}
	public void setTotalDollarValume(long totalDollarValume) {
		this.totalDollarValume = totalDollarValume;
	}
	public void setTotalDollarValume(String totalDollarValume) {
		this.totalDollarValume = Long.parseLong(totalDollarValume);
	}
	
	
	public long getAvgPrice() {
		return avgPrice;
	}
	public void setAvgPrice(long avgPrice) {
		this.avgPrice = avgPrice;
	}
	public void setAvgPrice(String avgPrice) {
		this.avgPrice = Long.parseLong(avgPrice);
	}
	
	
	public long getMedianPrice() {
		return medianPrice;
	}
	public void setMedianPrice(long medianPrice) {
		this.medianPrice = medianPrice;
	}
	public void setMedianPrice(String medianPrice) {
		this.medianPrice = Long.parseLong(medianPrice);
	}
	
	
	public int getNewListing() {
		return newListing;
	}
	public void setNewListing(int newListing) {
		this.newListing = newListing;
	}
	public void setNewListing(String newListing) {
		this.newListing = ((Double) Double.parseDouble(newListing)).intValue();
	}
	
	
	public double getSnlr() {
		return snlr;
	}
	public void setSnlr(double snlr) {
		this.snlr = snlr;
	}
	public void setSnlr(String snlr) {
		this.snlr = Double.parseDouble(snlr);
	}
	
	
	public int getActiveListings() {
		return activeListings;
	}
	public void setActiveListings(int activeListings) {
		this.activeListings = activeListings;
	}
	public void setActiveListings(String activeListings) {
		this.activeListings = ((Double) Double.parseDouble(activeListings)).intValue();
	}
	
	
	public double getMosInv() {
		return mosInv;
	}
	public void setMosInv(double mosInv) {
		this.mosInv = mosInv;
	}
	public void setMosInv(String mosInv) {
		this.mosInv = Double.parseDouble(mosInv);
	}
	
	
	public double getAvgSP_LP() {
		return avgSP_LP;
	}
	public void setAvgSP_LP(double avgSP_LP) {
		this.avgSP_LP = avgSP_LP;
	}
	public void setAvgSP_LP(String avgSP_LP) {
		this.avgSP_LP = Double.parseDouble(avgSP_LP);
	}
	
	
	public double getAvgDOM() {
		return avgDOM;
	}
	public void setAvgDOM(double avgDOM) {
		this.avgDOM = avgDOM;
	}
	public void setAvgDOM(String avgDOM) {
		this.avgDOM = Double.parseDouble(avgDOM);
	}
	
	
	
}
