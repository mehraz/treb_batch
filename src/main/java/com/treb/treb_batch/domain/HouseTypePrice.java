package com.treb.treb_batch.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "housetype_price")
public class HouseTypePrice {

	@Id
	private String id;
	private String houseTypeUniName;
	private Integer priceMin;
	private Integer priceMax;
	private Integer totalSale;
	private String date;

	public HouseTypePrice() {

	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public HouseTypePrice(String houseTypeUni, Integer priceMin, Integer priceMax, Integer totalSale, String date) {
		setAll(houseTypeUni, priceMin, priceMax, totalSale, date);
	}
	
	public void setAll(String houseTypeUni, Integer priceMin, Integer priceMax, Integer totalSale, String date) {
		this.houseTypeUniName = houseTypeUni;
		this.priceMin = priceMin;
		this.priceMax = priceMax;
		this.totalSale = totalSale;
		this.date = date;
		this.id = houseTypeUniName + date + "_" + priceMin + "_" + priceMax;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHouseTypeUniName() {
		return houseTypeUniName;
	}

	public void setHouseTypeUniName(String houseTypeUniName) {
		this.houseTypeUniName = houseTypeUniName;
	}

	public Integer getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(Integer priceMin) {
		this.priceMin = priceMin;
	}

	public Integer getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(Integer priceMax) {
		this.priceMax = priceMax;
	}

	public Integer getTotalSale() {
		return totalSale;
	}

	public void setTotalSale(Integer totalSale) {
		this.totalSale = totalSale;
	}

}
