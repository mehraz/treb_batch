package com.treb.treb_batch.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="region")
public class Region {
	
	@Id
	private String id;
	private String name;
	private String parent;
	private String description;
	
	@PersistenceConstructor
	public Region(String id, String name, String parent, String description){
		super();
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.description = description;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static String getUniName(String name){
		return name.toLowerCase().replaceAll("[.\\-,]", "").replaceAll(" ", "_");
	}
}
