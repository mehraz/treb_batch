package com.treb.treb_batch.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="house_type")
public class HouseType {

	
	@Id
	private String id;
	private String uniName;
	private String name;
	
	@PersistenceConstructor
	public HouseType(String id, String uniName, String name){
		super();
		this.id = id;
		this.uniName = uniName;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUniName() {
		return uniName;
	}
	public void setUniName(String uniName) {
		this.uniName = uniName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static enum TYPES { 
        DETACHED("detached"), 
        SEMI_DETACHED("semi_detached"), 
        ATT_ROW_TWNHSE("att_row_twnhse"), 
        CONDO_TWNHSE("condo_twnhse"), 
        CONDO_APT("condo_apt"), 
        DET_CONDO("det_condo"), 
        LINK("link"), 
        COOWNER_APT("coowner_apt"),
        COOP_APT("coop_apt"),
        ALL_TYPES("all_types");
		
        private String type; 
        private TYPES(String type) { 
            this.type = type; 
        } 
        
        @Override 
        public String toString(){ 
            return type; 
        } 
    } 
}
