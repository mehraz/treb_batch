package com.treb.treb_batch.domain;

import org.springframework.data.annotation.Id;


public class HouseRegionTransaction {
	
	@Id
	private String id;
	
	private String regionId;
	private String hTypeId;
	private Transaction trans;
	private String date;
	

	public HouseRegionTransaction(){
		trans = new Transaction();
		
	}
	
	public String generateId(){
		if(this.regionId==null || this.date==null){
			return null;
		}
		return this.regionId+"__"+this.date;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String gethTypeId() {
		return hTypeId;
	}

	public void sethTypeId(String hTypeId) {
		this.hTypeId = hTypeId;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	
	public Transaction getTrans() {
		return trans;
	}
	public void setTrans(Transaction trans) {
		this.trans = trans;
	}
	
	
}
