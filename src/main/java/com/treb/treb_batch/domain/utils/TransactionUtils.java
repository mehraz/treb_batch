package com.treb.treb_batch.domain.utils;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class TransactionUtils {
	
	private static final Logger logger =LoggerFactory.getLogger(TransactionUtils.class);


	private HashMap<String, String> transColMap;

	public TransactionUtils() {

	}

	@PostConstruct
	protected void init() {
		transColMap = createTransColEnum();
		logger.info("TransactionUtils: # of Cols added: "+ transColMap.size());
	}
	
	public String getTransPropByColName(String key) {
		if (transColMap.containsKey(key)) {
			return transColMap.get(key);
		}
		return "";
	}

	private HashMap<String, String> createTransColEnum() {
		HashMap<String, String> map = new HashMap<>();
		map.put("number of sales", "NumberOfSales");
		map.put("sales", "NumberOfSales");
		map.put("dollar volume", "TotalDollarValume");
		map.put("active listings", "ActiveListings");
		map.put("average price", "AvgPrice");
		map.put("avg. price", "AvgPrice");
		map.put("avg price", "AvgPrice");
		map.put("median price", "MedianPrice");
		map.put("new listings", "NewListing");
		map.put("snlr (trend)", "Snlr");
		map.put("snlr (trend)", "Snlr");
		map.put("snlr", "Snlr");
		map.put("mos. inv. (trend)", "MosInv");
		map.put("mos inv (trend)", "MosInv");
		map.put("avg. sp / lp", "AvgSP_LP");
		map.put("avg sp / lp", "AvgSP_LP");
		map.put("avg sp/lp", "AvgSP_LP");
		map.put("avgsp/lp", "AvgSP_LP");
		map.put("avg splp", "AvgSP_LP");
		map.put("average sp / lp", "AvgSP_LP");
		map.put("avg dom", "AvgDOM");
		map.put("avg. dom", "AvgDOM");
		map.put("average dom", "AvgDOM");
		
		return map;
	}

}
