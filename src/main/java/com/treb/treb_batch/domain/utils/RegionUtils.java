package com.treb.treb_batch.domain.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.treb.treb_batch.dao.regionRepository;
import com.treb.treb_batch.domain.Region;

@Component
public class RegionUtils {
	
	private static final Logger logger =LoggerFactory.getLogger(RegionUtils.class);

	@Autowired
	regionRepository regionRepo;

	private HashMap<String, Region> regionNameMap;

	public RegionUtils() {

	}

	@PostConstruct
	protected void init() {
		insertInitialData();
		regionNameMap = createRegionNameEnum();
		logger.info("RegionUtils: # of regions read from db: "+ regionNameMap.size());
	}

	public int insertInitialData() {
		List<Region> regions = provideActualRegions();

		int cnt = 0;
		for (Region rgn : regions) {
			if (!regionRepo.searchByName(rgn.getName()).iterator().hasNext()) {
				regionRepo.save(rgn);
			}
		}
		return cnt;
	}

	public String getRegoinUniByName(String key) {
		if (regionNameMap.containsKey(key)) {
			return regionNameMap.get(key).getId();
		}
		return "";
	}

	private HashMap<String, Region> createRegionNameEnum() {
		Iterable<Region> result = regionRepo.findAll();
		HashMap<String, Region> map = new HashMap<String, Region>();
		Iterator<Region> it = result.iterator();
		
		while (it.hasNext()) {
			Region tmp = it.next();
			map.put(tmp.getName(), tmp);
		}
		
		//default values
		map.put("TREB Total", map.get("GTA"));
		
		return map;
	}

	/**
	 * return a list of regions registered in treb website
	 * 
	 * @return
	 */
	private List<Region> provideActualRegions() {

		List<Region> types = new ArrayList<Region>();
		
		types.add(createNewObj("Ontario", "canada"));
		types.add(createNewObj("GTA", "ontario"));
		types.add(createNewObj("Halton Region", "gta"));
		
		types.add(createNewObj("Peel Region", "gta"));
		types.add(createNewObj("Toronto", "gta"));
		types.add(createNewObj("York Region", "gta"));
		types.add(createNewObj("Durham Region", "gta"));
		types.add(createNewObj("Dufferin County", "gta"));
		types.add(createNewObj("Simcoe County", "gta"));
		types.add(createNewObj("Toronto West", "Toronto"));
		types.add(createNewObj("Toronto Central", "Toronto"));
		types.add(createNewObj("Toronto East", "Toronto"));
		types.add(createNewObj("City of Toronto", "Toronto"));
		types.add(createNewObj("City of Toronto Total", "Toronto"));
		
		
		types.add(createNewObj("Burlington", "halton_region"));
		types.add(createNewObj("Halton Hills", "halton_region"));
		types.add(createNewObj("Milton", "halton_region"));
		types.add(createNewObj("Oakville", "halton_region"));
		
		types.add(createNewObj("Brampton", "peel_region"));
		types.add(createNewObj("Caledon", "peel_region"));
		types.add(createNewObj("Mississauga", "peel_region"));

		types.add(createNewObj("Toronto West", "toronto"));
		types.add(createNewObj("Toronto Central", "toronto"));
		types.add(createNewObj("Toronto East", "toronto"));
		
		types.add(createNewObj("Aurora", "york_region"));
		types.add(createNewObj("E. Gwillimbury", "york_region"));
		types.add(createNewObj("Georgina", "york_region"));
		types.add(createNewObj("King", "york_region"));
		types.add(createNewObj("Markham", "york_region"));
		types.add(createNewObj("Newmarket", "york_region"));
		types.add(createNewObj("Richmond Hill", "york_region"));
		types.add(createNewObj("Vaughan", "york_region"));
		types.add(createNewObj("Whitchurch-Stouffville", "york_region"));
		
		types.add(createNewObj("Ajax", "durham_region"));
		types.add(createNewObj("Brock", "durham_region"));
		types.add(createNewObj("Clarington", "durham_region"));
		types.add(createNewObj("Oshawa", "durham_region"));
		types.add(createNewObj("Pickering", "durham_region"));
		types.add(createNewObj("Scugog", "durham_region"));
		types.add(createNewObj("Uxbridge", "durham_region"));
		types.add(createNewObj("Whitby", "durham_region"));
		
		types.add(createNewObj("Orangeville", "dufferin_county"));
		
		types.add(createNewObj("Adjala-Tosorontio", "simcoe_county"));
		types.add(createNewObj("Bradford West", "simcoe_county"));
		types.add(createNewObj("Essa", "simcoe_county"));
		types.add(createNewObj("Innisfil", "simcoe_county"));
		types.add(createNewObj("New Tecumseth", "simcoe_county"));
		
		types.add(createNewObj("Toronto W01", "toronto_west"));
		types.add(createNewObj("Toronto W02", "toronto_west"));
		types.add(createNewObj("Toronto W03", "toronto_west"));
		types.add(createNewObj("Toronto W04", "toronto_west"));
		types.add(createNewObj("Toronto W05", "toronto_west"));
		types.add(createNewObj("Toronto W06", "toronto_west"));
		types.add(createNewObj("Toronto W07", "toronto_west"));
		types.add(createNewObj("Toronto W08", "toronto_west"));
		types.add(createNewObj("Toronto W09", "toronto_west"));
		types.add(createNewObj("Toronto W10", "toronto_west"));
		
		types.add(createNewObj("Toronto C01", "toronto_central"));
		types.add(createNewObj("Toronto C02", "toronto_central"));
		types.add(createNewObj("Toronto C03", "toronto_central"));
		types.add(createNewObj("Toronto C04", "toronto_central"));
		types.add(createNewObj("Toronto C05", "toronto_central"));
		types.add(createNewObj("Toronto C06", "toronto_central"));
		types.add(createNewObj("Toronto C07", "toronto_central"));
		types.add(createNewObj("Toronto C08", "toronto_central"));
		types.add(createNewObj("Toronto C09", "toronto_central"));
		types.add(createNewObj("Toronto C10", "toronto_central"));
		types.add(createNewObj("Toronto C11", "toronto_central"));
		types.add(createNewObj("Toronto C12", "toronto_central"));
		types.add(createNewObj("Toronto C13", "toronto_central"));
		types.add(createNewObj("Toronto C14", "toronto_central"));
		types.add(createNewObj("Toronto C15", "toronto_central"));
		
		types.add(createNewObj("Toronto E01", "toronto_east"));
		types.add(createNewObj("Toronto E02", "toronto_east"));
		types.add(createNewObj("Toronto E03", "toronto_east"));
		types.add(createNewObj("Toronto E04", "toronto_east"));
		types.add(createNewObj("Toronto E05", "toronto_east"));
		types.add(createNewObj("Toronto E06", "toronto_east"));
		types.add(createNewObj("Toronto E07", "toronto_east"));
		types.add(createNewObj("Toronto E08", "toronto_east"));
		types.add(createNewObj("Toronto E09", "toronto_east"));
		types.add(createNewObj("Toronto E10", "toronto_east"));
		types.add(createNewObj("Toronto E11", "toronto_east"));
		
		return types;
	}
	
	private Region createNewObj(String name, String parent){
		Region rgn = new Region(Region.getUniName(name), name, parent, "");
		return rgn;
	}
}
