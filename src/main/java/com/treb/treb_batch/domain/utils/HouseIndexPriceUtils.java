package com.treb.treb_batch.domain.utils;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class HouseIndexPriceUtils {
	private static final Logger logger =LoggerFactory.getLogger(HouseIndexPriceUtils.class);


	private HashMap<String, String> hipColMap;
	

	public HouseIndexPriceUtils() {

	}

	@PostConstruct
	protected void init() {
		hipColMap = createIndexTableColEnum();
		logger.info("# of Cols added: "+ hipColMap.size());
	}
	
	public String getIndexPricePropByColName(String key) {
		if (hipColMap.containsKey(key)) {
			return hipColMap.get(key);
		}
		return "";
	}

	private HashMap<String, String> createIndexTableColEnum() {
		HashMap<String, String> map = new HashMap<>();
		map.put("index", "Index");
		map.put("benchmark", "Benchmark");
		map.put("yryr chg", "AnnuallPriceChange");
		map.put("yryr  chg", "AnnuallPriceChange");
		map.put("yr/yr % chg", "AnnuallPriceChange");
		
		
		return map;
	}

}
