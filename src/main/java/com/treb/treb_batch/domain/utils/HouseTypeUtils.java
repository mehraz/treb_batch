package com.treb.treb_batch.domain.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.treb.treb_batch.dao.HouseTypeRepository;
import com.treb.treb_batch.domain.HouseType;

@Component
public class HouseTypeUtils {
	
	@Autowired
	HouseTypeRepository houseTypeRepo;
	
	private HashMap<String, String> houseTypeNameEnum;
	
	public HouseTypeUtils(){

	}
	
	@PostConstruct
	protected void init(){
		//insertInitialData();
		houseTypeNameEnum = createHouseTypeNameEnum();
	}
	
	
	public int insertInitialData(){
		List<HouseType> houseTypes = provideActualHouseType();
		
		int cnt = 0;
		for(HouseType type: houseTypes){
			if(!houseTypeRepo.searchByName(type.getName()).iterator().hasNext()){
				houseTypeRepo.save(type);
			}
		}
		return cnt;
	}
	
	public String getHouseTypeUniByName(String key){
		if(houseTypeNameEnum.containsKey(key)){
			return houseTypeNameEnum.get(key);
		}
		return "";
	}
	
private HashMap<String, String> createHouseTypeNameEnum(){
		Iterable<HouseType> result = houseTypeRepo.findAll();
		HashMap<String, String> map = new HashMap<String, String>() ;
		Iterator<HouseType> it = result.iterator();
		HouseType tmp;
		while(it.hasNext()){
			tmp = it.next();
			map.put(tmp.getName(), tmp.getUniName());
		}
		return map;
	}
	
	
	/**
	 * return a list of house type registered in treb website
	 * @return
	 */
	private List<HouseType> provideActualHouseType(){
		
		List<HouseType> types = new ArrayList<HouseType>();
		types.add(new HouseType(new ObjectId().toString(), "detached", "Detached"));
		types.add(new HouseType(new ObjectId().toString(), "semi_detached", "Semi-Detached"));
		types.add(new HouseType(new ObjectId().toString(), "att_row_twnhse", "Att/Row/Twnhouse"));
		types.add(new HouseType(new ObjectId().toString(), "condo_twnhse", "Condo Townhouse"));
		types.add(new HouseType(new ObjectId().toString(), "condo_apt", "Condo Apt"));
		types.add(new HouseType(new ObjectId().toString(), "link", "Link"));
		types.add(new HouseType(new ObjectId().toString(), "coop_apt", "Co-op Apt"));
		types.add(new HouseType(new ObjectId().toString(), "det_condo", "Det Condo"));
		types.add(new HouseType(new ObjectId().toString(), "coowner_apt", "Co-ownership Apt"));
		types.add(new HouseType(new ObjectId().toString(), "all_types", "all types"));
		//from INdex Table
		
		types.add(new HouseType(new ObjectId().toString(), "all_types", "Composite"));
		types.add(new HouseType(new ObjectId().toString(), "detached", "Single-Family Detached"));
		types.add(new HouseType(new ObjectId().toString(), "semi_detached", "Single-Family Attached"));
		types.add(new HouseType(new ObjectId().toString(), "att_row_twnhse", "Townhouse"));
		types.add(new HouseType(new ObjectId().toString(), "condo_apt", "Apartment"));
		return types;
	}
}
