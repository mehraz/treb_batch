package com.treb.treb_batch.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="house_index_price")
public class HouseIndexPrice {
	
	@Id
	String id;
	String regionId;
	String houseTypeId;
	double index;
	long benchmark;
	double annuallPriceChange;
	String date;
	
	public String generateId(){
		if(this.regionId==null || this.date==null || this.houseTypeId == null){
			return null;
		}
		return this.regionId+"__" + this.houseTypeId +"__"+this.date;
	}
	
	public double getAnnuallPriceChange() {
		return annuallPriceChange;
	}


	public void setAnnuallPriceChange(double annuallPriceChange) {
		this.annuallPriceChange = annuallPriceChange;
	}
	
	public void setAnnuallPriceChange(String annuallPriceChange) {
		this.annuallPriceChange = Double.parseDouble(annuallPriceChange);
	}
	
	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getHouseTypeId() {
		return houseTypeId;
	}
	public void setHouseTypeId(String houseTypeId) {
		this.houseTypeId = houseTypeId;
	}
	public double getIndex() {
		return index;
	}
	public void setIndex(double index) {
		this.index = index;
	}
	public void setIndex(String index) {
		this.index = Double.parseDouble(index);
	}
	public long getBenchmark() {
		return benchmark;
	}
	public void setBenchmark(long benchmark) {
		this.benchmark = benchmark;
	}

	public void setBenchmark(String benchmark) {
		this.benchmark = Long.parseLong(benchmark);
	}

}
