package com.treb.treb_batch.config;

import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.dao.MapExecutionContextDao;
import org.springframework.batch.core.repository.dao.MapJobExecutionDao;
import org.springframework.batch.core.repository.dao.MapJobInstanceDao;
import org.springframework.batch.core.repository.dao.MapStepExecutionDao;
import org.springframework.batch.core.repository.support.SimpleJobRepository;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.context.annotation.*;

@Configuration
//@PropertySource("classpath:mainProp.properties")
//@EnableBatchProcessing
public class ScheduleConfig {
	/*
	@Bean
	public ItemReader<String> reader(){
		ItemReader<String> read = new SimpleRead();
		return read;
	}
	
	@Bean
	public ItemWriter<String> writer(){
		ItemWriter<String> wr = new SimpleWrite();
		return wr;
	}
	*/
	@Bean
	public SimpleJobRepository jobRepository(){
		SimpleJobRepository jr = new SimpleJobRepository(new MapJobInstanceDao(),new MapJobExecutionDao(),new MapStepExecutionDao(),new MapExecutionContextDao());
		return jr;
	}
	
	@Bean
	public SimpleJobLauncher jobLauncher()
	{
		SimpleJobLauncher jl = new SimpleJobLauncher();
		jl.setJobRepository(jobRepository());
		return jl;
	}
	
	@Bean
	public ResourcelessTransactionManager transactionManager(){
		return new ResourcelessTransactionManager();
	}
}
