package com.treb.treb_batch.config;


import java.util.ArrayList;
	
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
@EnableMongoRepositories(basePackages = "com.treb.treb_batch.dao")
public class AppMongoDBConfig extends AbstractMongoConfiguration{

	@Autowired
	TrebConfig trebConfig;
	
	
	@Override
	protected String getDatabaseName() {

		return trebConfig.getDbName();
	}

	@Override
	@Bean
	public Mongo mongo() throws Exception {
		
			MongoCredential trebAuth = MongoCredential.createMongoCRCredential(trebConfig.getDbUserName(), trebConfig.getDbName(), trebConfig.getDbPassword().toCharArray());
			ArrayList<MongoCredential> auths = new ArrayList<MongoCredential>();
			auths.add(trebAuth);

			ServerAddress serverAddress = new ServerAddress(trebConfig.getDbAddress(), trebConfig.getDbPort());
			return new MongoClient(serverAddress, auths);			
	}
	
	@Bean
	public MongoDbFactory mongoDbFactory() throws Exception {
		return new SimpleMongoDbFactory(mongo(), getDatabaseName());
	  }
	
	@Override
	@Bean
	public MongoTemplate mongoTemplate() throws Exception{
		//remove _class
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
		MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
			//new MappingMongoConverter(mongoDbFactory(), new MongoMappingContext());
		converter.setTypeMapper(new DefaultMongoTypeMapper(null));

		MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);

		return mongoTemplate;
	}
	
	@Override
	 protected String getMappingBasePackage() {
	    return "com.treb.treb_batch.dao";
	  }
	
}
