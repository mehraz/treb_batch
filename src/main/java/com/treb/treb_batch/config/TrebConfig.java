package com.treb.treb_batch.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({ @PropertySource(value = "classpath:/mainProp.properties", ignoreResourceNotFound = true)})
public class TrebConfig {
	
	@Value("${datasource.mongo.name}")
	private String dbName;
	
	@Value("${datasource.mongo.port}")
	private int dbPort;
	
	@Value("${datasource.mongo.address}")
	private String dbAddress;
	
	@Value("${datasource.mongo.username}")
	private String dbUserName;
	
	@Value("${datasource.mongo.password}")
	private String dbPassword;
	
	@Value("${excel.json.template.regex}")
	private String jsonTemplateRegex;
	
	@Value("${excel.json.template.directory}")
	private String jsonTemplateDirectory;
	
	@Value("${excel.data.date.regex}")
	private String excelDateRegex;
	
	@Value("${schedule.treb.excel.donefolder}")
	private String trebExcelDoneFolder;
	
	@Value("${schedule.treb.excel.readfolder}")
	private String trebExcelReadFolder;
	
	@Value("${schedule.treb.excel.failedfolder}")
	private String trebExcelFailedFolder;
	


	public String getTrebExcelFailedFolder() {
		return trebExcelFailedFolder;
	}

	public void setTrebExcelFailedFolder(String trebExcelFailedFolder) {
		this.trebExcelFailedFolder = trebExcelFailedFolder;
	}

	public String getTrebExcelDoneFolder() {
		return trebExcelDoneFolder;
	}

	public void setTrebExcelDoneFolder(String trebExcelDoneFolder) {
		this.trebExcelDoneFolder = trebExcelDoneFolder;
	}

	public String getTrebExcelReadFolder() {
		return trebExcelReadFolder;
	}

	public void setTrebExcelReadFolder(String trebExcelReadFolder) {
		this.trebExcelReadFolder = trebExcelReadFolder;
	}

	public String getExcelDateRegex() {
		return excelDateRegex;
	}

	public void setExcelDateRegex(String excelDateRegex) {
		this.excelDateRegex = excelDateRegex;
	}

	public String getJsonTemplateDirectory() {
		return jsonTemplateDirectory;
	}

	public void setJsonTemplateDirectory(String jsonTemplateDirectory) {
		this.jsonTemplateDirectory = jsonTemplateDirectory;
	}

	public String getJsonTemplateRegex() {
		return jsonTemplateRegex;
	}

	public void setJsonTemplateRegex(String jsonTemplateRegex) {
		this.jsonTemplateRegex = jsonTemplateRegex;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public int getDbPort() {
		return dbPort;
	}

	public void setDbPort(String dbPort) {
		this.dbPort = Integer.parseInt(dbPort);
	}
	
	public void setDbPort(int dbPort) {
		this.dbPort = dbPort;
	}

	public String getDbAddress() {
		return dbAddress;
	}

	public void setDbAddress(String dbAddress) {
		this.dbAddress = dbAddress;
	}
	
	public TrebConfig(){

	}
	
}
