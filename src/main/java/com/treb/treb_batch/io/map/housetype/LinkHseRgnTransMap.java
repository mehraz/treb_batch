package com.treb.treb_batch.io.map.housetype;

import org.springframework.stereotype.Component;

import com.treb.treb_batch.domain.HouseRegionTransaction;
import com.treb.treb_batch.domain.HouseType;

@Component
public class LinkHseRgnTransMap extends HouseRegionTransactionMap {
	@Override
	protected HouseRegionTransaction setHouseType(HouseRegionTransaction converted){
		converted.sethTypeId(HouseType.TYPES.LINK.toString());
		return converted;
	}
	
	@Override
	protected String getCollectionName(){
		return "linkRegionTransaction";
	}
}
