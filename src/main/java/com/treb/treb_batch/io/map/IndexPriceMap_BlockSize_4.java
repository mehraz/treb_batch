package com.treb.treb_batch.io.map;

import org.springframework.stereotype.Component;

@Component
public class IndexPriceMap_BlockSize_4 extends IndexPriceMap {
	
	public IndexPriceMap_BlockSize_4(){
		SECTION_SIZE = 4;
	}
}
