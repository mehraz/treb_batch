package com.treb.treb_batch.io.map.housetype;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.treb.treb_batch.domain.HouseRegionTransaction;
import com.treb.treb_batch.domain.HouseType;
import com.treb.treb_batch.domain.Transaction;
import com.treb.treb_batch.domain.utils.RegionUtils;
import com.treb.treb_batch.domain.utils.TransactionUtils;
import com.treb.treb_batch.io.map.TableToEntityMap;

@Primary
@Configuration
@Qualifier("totalHouseRegionTransactionMap")
public class HouseRegionTransactionMap implements TableToEntityMap<HouseRegionTransaction> {
	
	private static final Logger logger =LoggerFactory.getLogger(HouseRegionTransactionMap.class);
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	RegionUtils regionUtils;
	
	@Autowired
	TransactionUtils transutils;

	
	@Override
	public ArrayList<HouseRegionTransaction> mapAll(ArrayList<ArrayList<String>> data) {
		if(data == null) return null;
		ArrayList<HouseRegionTransaction> result = new ArrayList<HouseRegionTransaction>();
		
		for (int i = 1; i < data.size(); i++) {
			
			HouseRegionTransaction converted = new HouseRegionTransaction();
			converted = setHRT_GeneralProps(data,i,converted);
			for (int j = 1; j < data.get(i).size(); j++) {			
				converted = mapOne(data,converted, i, j);
			}
			result.add(converted);
		}
		return result;
	}
	

	
	@Override
	public HouseRegionTransaction mapOne(ArrayList<ArrayList<String>> data, HouseRegionTransaction converted, int i, int j) {
		
		if(data == null || data.size()<2){
			return null;
		}
		
		if(i<=0 || i>=data.size() || j<=0 || j>=data.get(0).size()) {
			return converted;
		}
		
		
		int headerIndex = j;
		if(data.get(0).size()< data.get(j).size() && ((headerIndex+ 1)< data.get(0).size())){
			headerIndex++; //to fix the issue of one cell off in headers for pds older than 20150630
		}
		
		
		String colName = data.get(0).get(headerIndex).toLowerCase().replaceAll("[0-9\\.,]", "").trim();
		String transPropName = transutils.getTransPropByColName(colName);
		
		if(transPropName==""){
			logger.debug("transPropName is empty for '"+data.get(0).get(j)+"'");
		}
		
		Method setMethod;
		String val = data.get(i).get(j).replaceAll("[,$%]", "").trim();
		if(val.equals("") || val.equals("-")){
			val = "0";
		}
	
		try {
			setMethod = Transaction.class.getMethod("set"+transPropName, new Class[]{String.class});
			setMethod.invoke(converted.getTrans(), val);
			
		} catch (NoSuchMethodException | SecurityException e) {
			
			if(colName!=""){
				logger.info("failed to get setMethod '"+transPropName+"'. colName:" + colName +". value:" + val);
				logger.error("failed to get setMethod."+e.getMessage());
			}
			
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			
			if(colName!=""){
				logger.info("failed to invoke setMethod '"+transPropName+"'. colName:" + colName +". value:" + val);
				logger.error("failed to invoke setMethod.'"+transPropName+"'. Detail:"+e.getMessage());
				e.printStackTrace();
			}
		}

		return converted;
	}
	
	

	
	@Override
	public int streamMapToDB(ArrayList<ArrayList<String>> data) {
		if(data==null){
			logger.debug("streamMapToDB, data is empty.");
			return 0;
		}
		int cnt = 0;
		for (int i = 1; i < data.size(); i++) {
			
			HouseRegionTransaction converted = new HouseRegionTransaction();
			converted = setHRT_GeneralProps(data,i,converted);
			if(converted == null || converted.getRegionId() == null || converted.getRegionId() == ""){
				//ignore since it will be empty row
				continue;
			}
			
			//set Transaction values
			for (int j = 1; j < data.get(i).size(); j++) {	
				converted = mapOne(data,converted, i, j);
			}
			
			if (isDataUnique(converted)) {
				//repo.save(converted);
				mongoTemplate.insert(converted, getCollectionName());
				cnt++;
			}
		}
		return cnt;
	}
	
	
	protected HouseRegionTransaction setHouseType(HouseRegionTransaction converted){
		converted.sethTypeId(HouseType.TYPES.ALL_TYPES.toString());
		return converted;
	}
	
	protected String getCollectionName(){
		return "houseRegionTransaction";
	}

	
	protected HouseRegionTransaction setHRT_GeneralProps(ArrayList<ArrayList<String>> data, int i, HouseRegionTransaction converted){
		String regionName = data.get(i).get(0);
		if(regionName == "") {
			return null;
		}
		String regionId = regionUtils.getRegoinUniByName(regionName);
		
		if(regionId ==""){
			logger.info("regionId is empty for '"+regionName+"'");
			regionId = regionName;
		}
		converted.setDate(data.get(0).get(0));
		converted.setRegionId(regionId);
		converted = setHouseType(converted);
		converted.setId(converted.generateId());
		
		return converted;
	}

	
	@Override
	public boolean isDataUnique(HouseRegionTransaction data) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(data.getId()));
		return !mongoTemplate.exists(query, getCollectionName());
	}

}
