package com.treb.treb_batch.io.map.housetype;

import org.springframework.context.annotation.Configuration;

import com.treb.treb_batch.domain.HouseRegionTransaction;
import com.treb.treb_batch.domain.HouseType;

@Configuration
public class DetachedCondoRgnTransMap extends HouseRegionTransactionMap {
	
	@Override
	protected HouseRegionTransaction setHouseType(HouseRegionTransaction converted){
		converted.sethTypeId(HouseType.TYPES.DET_CONDO.toString());
		return converted;
	}
	
	@Override
	protected String getCollectionName(){
		return "detachedCondoRegionTransaction";
	}
}
