package com.treb.treb_batch.io.map;

import java.util.ArrayList;



public interface TableToEntityMap <T> {
	
	public ArrayList<T> mapAll(ArrayList<ArrayList<String>> data);
	public T mapOne(ArrayList<ArrayList<String>> data, T obj, int i, int j);
	public int streamMapToDB(ArrayList<ArrayList<String>> data);
	
	public boolean isDataUnique(T data);
}
