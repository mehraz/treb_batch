package com.treb.treb_batch.io.map.housetype;

import org.springframework.context.annotation.Configuration;

import com.treb.treb_batch.domain.HouseRegionTransaction;
import com.treb.treb_batch.domain.HouseType;

@Configuration
public class DetachedHouseRegionTransMap extends HouseRegionTransactionMap {
	
	@Override
	protected HouseRegionTransaction setHouseType(HouseRegionTransaction converted){
		converted.sethTypeId(HouseType.TYPES.DETACHED.toString());
		return converted;
	}
	
	@Override
	protected String getCollectionName(){
		return "detachedHseRegionTransaction";
	}
}
