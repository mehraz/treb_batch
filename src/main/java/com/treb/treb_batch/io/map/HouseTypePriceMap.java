package com.treb.treb_batch.io.map;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.treb.treb_batch.dao.HouseTypePriceRepository;
import com.treb.treb_batch.domain.HouseTypePrice;
import com.treb.treb_batch.domain.utils.HouseTypeUtils;


@Configuration
public class HouseTypePriceMap implements TableToEntityMap<HouseTypePrice> {
	
	private static final Logger logger =LoggerFactory.getLogger(HouseTypePriceMap.class);

	@Autowired
	HouseTypePriceRepository repo;
	
	
	@Autowired
	HouseTypeUtils houseTypeUtils;

	public HouseTypePriceMap() {

	}
	
	@Override
	public ArrayList<HouseTypePrice> mapAll(ArrayList<ArrayList<String>> data) {
		if(data == null) return null;
		ArrayList<HouseTypePrice> result = new ArrayList<HouseTypePrice>();
		
		for (int i = 1; i < data.size(); i++) {

			for (int j = 1; j < data.get(i).size(); j++) {
				HouseTypePrice converted = new HouseTypePrice();
				converted = mapOne(data,converted, i, j);
				result.add(converted);
			}
		}
		return result;
	}
	
	@Override
	public HouseTypePrice mapOne(ArrayList<ArrayList<String>> data,HouseTypePrice converted,  int i, int j) {
		
		if(data == null || data.size()<2){
			return null;
		}
		if(i<=0 || i>=data.size() || j<=0 || j>=data.size()) {
			return null;
		}
		
		String houseType = data.get(0).get(j);
		String houseTypeUni = houseTypeUtils.getHouseTypeUniByName(houseType);
		
		if(houseTypeUni ==""){
			logger.debug("HouseTypePrice: houseTypeUni is empty for '"+houseType+"'. > at row: " + i + "column" + j);
			houseTypeUni = houseType;
		}
		String cleanedUpPriceRange = cleanUpNumberStr(data.get(i).get(0)).replaceAll("to", "-").trim();
		if(cleanedUpPriceRange == ""){
			return null;
		}
		String[] priceRange = cleanUpNumberStr(data.get(i).get(0)).replaceAll("to", "-").trim().split("-");
		
		
		
		Integer totalSale;
		try{
			totalSale = (int)Double.parseDouble(cleanUpNumberStr(data.get(i).get(j)));
		} catch (Exception e){
			logger.error("Failed total sale for price range:"+cleanedUpPriceRange + " >> at row: " + i + "column" + j);
			return null;
		}
		String dateStr = data.get(0).get(0);
		String minPrice = "0";
		String maxPrice = Double.toString(Double.MAX_VALUE);
		if(priceRange!= null && priceRange.length>0){
			minPrice = priceRange[0];
		}
		if(priceRange!= null && priceRange.length>1){
			maxPrice = priceRange[1];
		}
		try{
		converted.setAll(houseTypeUni, (int)Double.parseDouble(minPrice),
				(int)Double.parseDouble(maxPrice), totalSale, dateStr);
		} catch(Exception e){
			logger.error("failed parsing " + minPrice + "or "+maxPrice + " --- originalRange: " + cleanedUpPriceRange);
		}
		return converted;
	}
	
	private String cleanUpNumberStr(String num){
		return num.replaceAll("[$, \\+]+", "");
	}
	
	@Override
	public int streamMapToDB(ArrayList<ArrayList<String>> data) {
		if(data==null){
			//log that data is null
			return 0;
		}
		int cnt = 0;
		for (int i = 1; i < data.size(); i++) {

			for (int j = 1; j < data.get(i).size(); j++) {
				HouseTypePrice converted = new HouseTypePrice();
				converted = mapOne(data,converted, i, j);
				if(converted == null){
					continue;
				}
				if (isDataUnique(converted)) {
					repo.save(converted);
					cnt++;
				}
			}
		}
		return cnt;
	}

	@Override
	public boolean isDataUnique(HouseTypePrice data) {
		if(data ==null) return false;
		return !repo.exists(data.getId());
	}

}
