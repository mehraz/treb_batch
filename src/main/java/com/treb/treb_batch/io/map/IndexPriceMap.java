package com.treb.treb_batch.io.map;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.treb.treb_batch.dao.HouseIndexPriceRepository;
import com.treb.treb_batch.domain.HouseIndexPrice;
import com.treb.treb_batch.domain.utils.HouseIndexPriceUtils;
import com.treb.treb_batch.domain.utils.HouseTypeUtils;
import com.treb.treb_batch.domain.utils.RegionUtils;

@Configuration
public class IndexPriceMap implements TableToEntityMap<HouseIndexPrice> {
	
	
	private static final Logger logger =LoggerFactory.getLogger(IndexPriceMap.class);

	@Autowired
	HouseIndexPriceRepository repo;
	
	@Autowired
	RegionUtils regionUtils;
	
	@Autowired
	HouseTypeUtils houseTypeUtils;
	
	
	@Autowired
	HouseIndexPriceUtils housePriceIndexUtils;
	
	public static int SECTION_SIZE = 3;
	
	
	@Override
	public ArrayList<HouseIndexPrice> mapAll(ArrayList<ArrayList<String>> data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HouseIndexPrice mapOne(ArrayList<ArrayList<String>> data, HouseIndexPrice converted, int i, int j) {
		
		if(data == null || data.size()<2){
			return null;
		}
		
		if(i<=0 || i>=data.size() || j<=0 || j>=data.size()) {
			return null;
		}
		
		String colName = data.get(1).get(j).toLowerCase().replaceAll("[0-9\\.,/%]", "").trim();
		String indexPricePropName = housePriceIndexUtils.getIndexPricePropByColName(colName);
				
		if(indexPricePropName==""){
			logger.debug("indexPricePropName is empty for '"+data.get(0).get(j)+"'");
		}
		
		Method setMethod;
		String val = data.get(i).get(j).replaceAll("[,$%]", "").trim().split("\n")[0];
		if(val.equals("") || val.equals("-")){
			val = "0";
		}
	
		try {
			setMethod = HouseIndexPrice.class.getMethod("set"+indexPricePropName, new Class[]{String.class});
			setMethod.invoke(converted, val);
			
		} catch (NoSuchMethodException | SecurityException e) {
			if(colName!=""){
				logger.info("failed to get setMethod '"+indexPricePropName+"'. colName:" + colName +". value:" + val);
				logger.error("failed to get setMethod."+e.getMessage());
			}
			
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			if(colName!=""){
				logger.info("failed to invoke setMethod '"+indexPricePropName+"'. colName:" + colName +". value:" + val);
				logger.error("failed to invoke setMethod.'"+indexPricePropName+"'. Detail:"+e.getMessage());
				e.printStackTrace();
			}
		}

		return converted;
	}
	
	

	
	@Override
	public int streamMapToDB(ArrayList<ArrayList<String>> data) {
		if(data==null){
			logger.debug("streamMapToDB, data is empty.");
			return 0;
		}
		int cnt = 0;
		int propCount = 0;
		HouseIndexPrice converted;
		for (int i = 2; i < data.size(); i++) {
			
			converted = new HouseIndexPrice();
			
			propCount = 0;
			for (int j = 1; j < data.get(i).size(); j++) {	
				
				//each SECTION_SIZE columns becomes one object
				if(propCount== SECTION_SIZE){
					
					converted = setGeneralProps(data,i, j,converted);
					if(converted == null){
						continue;
					}
					//in case that two rows are merged
					
					if (isDataUnique(converted)) {
						repo.save(converted);
						cnt++;
					}
					propCount = 0;
					converted = new HouseIndexPrice();
					
				}
				converted = mapOne(data,converted, i, j);
				propCount++;
			}
			if(data.get(i).size()>0 && data.get(i).get(0).contains("\n")){
				removeFristMergedRow(data,  i);
				i--;
			}
			
			
		}
		return cnt;
	}
	
	private void removeFristMergedRow(ArrayList<ArrayList<String>> data, int i){
		for(int j=0; j<data.get(i).size();j++){
			String val = data.get(i).get(j);
			if(!val.contains("\n")) break;
			val = val.split("\n")[1];
			data.get(i).set(j, val);
		}
	}
	
	
	protected HouseIndexPrice setGeneralProps(ArrayList<ArrayList<String>> data, int i, int j, HouseIndexPrice converted){
		String regionName = data.get(i).get(0).split("\n")[0];
		if(regionName == ""){
			return null;
		}
		String regionId = regionUtils.getRegoinUniByName(regionName);
		
		if(regionId ==""){
			logger.info("regionId is empty for '"+regionName+"'");
			return null;
			//regionId = regionName;
		}
		converted.setDate(data.get(0).get(0));
		converted.setRegionId(regionId);
		converted.setHouseTypeId(getHouseTypeFromData(data, j));
		converted.setId(converted.generateId());
		
		return converted;
	}
	
	private String getHouseTypeFromData(ArrayList<ArrayList<String>> data, int j){
		String houseTypeVal = "";
		j-= SECTION_SIZE;
		houseTypeVal = data.get(0).get(j).split("\n")[0];
		
		
		String houseTypeId = houseTypeUtils.getHouseTypeUniByName(houseTypeVal);
		if(houseTypeId != null) {
			return houseTypeId; 
		}
		return houseTypeVal;
	}

	@Override
	public boolean isDataUnique(HouseIndexPrice data) {
		if(data == null) return false;
		return !repo.exists(data.getId());
	}

}
