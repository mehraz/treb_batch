package com.treb.treb_batch.io.map.housetype;

import org.springframework.stereotype.Component;

import com.treb.treb_batch.domain.HouseRegionTransaction;
import com.treb.treb_batch.domain.HouseType;

@Component
public class CondoAprHseRgnTransMap extends HouseRegionTransactionMap {
	@Override
	protected HouseRegionTransaction setHouseType(HouseRegionTransaction converted){
		converted.sethTypeId(HouseType.TYPES.CONDO_APT.toString());
		return converted;
	}
	
	@Override
	protected String getCollectionName(){
		return "condoAprtRegionTransaction";
	}
}
