package com.treb.treb_batch.io.map;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;

import com.treb.treb_batch.io.map.housetype.AttRowTwnRgnTransMap;
import com.treb.treb_batch.io.map.housetype.CoOwnrAptRgnTransMap;
import com.treb.treb_batch.io.map.housetype.CondoAprHseRgnTransMap;
import com.treb.treb_batch.io.map.housetype.CondoTwnHseRgnTransMap;
import com.treb.treb_batch.io.map.housetype.CoopRgnTransMap;
import com.treb.treb_batch.io.map.housetype.DetachedCondoRgnTransMap;
import com.treb.treb_batch.io.map.housetype.DetachedHouseRegionTransMap;
import com.treb.treb_batch.io.map.housetype.HouseRegionTransactionMap;
import com.treb.treb_batch.io.map.housetype.LinkHseRgnTransMap;
import com.treb.treb_batch.io.map.housetype.SemiDetachedHseRgnTransMap;


@Configuration
public class TableEntityMapFactory {
	
	@Autowired
	HouseTypePriceMap hseTPriceMap;
	
	@Autowired
	@Qualifier("totalHouseRegionTransactionMap")
	HouseRegionTransactionMap totalHouseRegionTransactionMap;
	
	@Autowired
	DetachedHouseRegionTransMap DetachedhseRegionTransMap;
	
	@Autowired
	SemiDetachedHseRgnTransMap semiDetachHTRMap;
	
	@Autowired
	CondoTwnHseRgnTransMap condoTwnHTRMap;
	
	@Autowired
	CondoAprHseRgnTransMap condoAprtHTRMap;
	
	@Autowired
	LinkHseRgnTransMap linkHTRMap;
	
	@Autowired
	AttRowTwnRgnTransMap artHRTMap;
	
	@Autowired
	CoopRgnTransMap coopHRTMap;
	
	@Autowired
	DetachedCondoRgnTransMap detachedCondoHRTMap;
	
	@Autowired
	IndexPriceMap indexPriceMap;
	
	@Autowired
	IndexPriceMap_BlockSize_4 indexPriceMap_BlockSize_4;
	
	
	@Autowired
	CoOwnrAptRgnTransMap coOwnerAptHRTMap;
	
	private HashMap<String, TableToEntityMap> map;
	
	@PostConstruct
	public void init(){
		map = new HashMap<>();
		map.put("housetype_price", hseTPriceMap);
		//
		map.put("housetrans_total", totalHouseRegionTransactionMap);
		map.put("housetrans_detachedhse", DetachedhseRegionTransMap);
		map.put("housetrans_semidetachedhse", semiDetachHTRMap);
		map.put("housetrans_condotwnhse", condoTwnHTRMap);	
		map.put("housetrans_condoaptr", condoAprtHTRMap);
		map.put("housetrans_link", linkHTRMap);
		map.put("housetrans_art", artHRTMap);
		map.put("housetrans_coop", coopHRTMap);
		map.put("housetrans_detachedcondo", detachedCondoHRTMap);
		map.put("housetrans_coowapt", coOwnerAptHRTMap);
		//
		map.put("house_index_price", indexPriceMap);
		map.put("house_index_price_block_size_4", indexPriceMap_BlockSize_4);
	}
	
	@SuppressWarnings("rawtypes")
	public TableToEntityMap getMapper(String name){
		if(name == null){
			return null;
		}
		name = name.toLowerCase();
		if(map.containsKey(name)){
			return map.get(name);
		}
		return null;
	}
}
