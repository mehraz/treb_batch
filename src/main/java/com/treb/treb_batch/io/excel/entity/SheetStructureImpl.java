package com.treb.treb_batch.io.excel.entity;

import java.util.ArrayList;

public class SheetStructureImpl implements SheetStructure {
	
	private int sheetNum;
	private String name;
	private ArrayList<TableBoundaryImpl> tableBounderies;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public SheetStructureImpl(){
		tableBounderies = new ArrayList<TableBoundaryImpl>();
	}

	public int getSheetNum() {
		return sheetNum;
	}

	public void setSheetNum(int sheetNum) {
		this.sheetNum = sheetNum;
	}

	public ArrayList<TableBoundaryImpl> getTableBounderies() {
		return tableBounderies;
	}

	public void setTableBounderies(ArrayList<TableBoundaryImpl> tableBounderies) {
		this.tableBounderies = tableBounderies;
	}
}
