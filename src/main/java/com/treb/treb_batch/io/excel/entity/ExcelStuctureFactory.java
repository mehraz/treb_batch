package com.treb.treb_batch.io.excel.entity;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.treb.treb_batch.config.TrebConfig;

@Component
public class ExcelStuctureFactory {
	
	private static final Logger logger =LoggerFactory.getLogger(ExcelStuctureFactory.class);

	@Autowired
	private TrebConfig config;

	private HashMap<String, ExcelStrcutureImpl> registeredTemplates = new HashMap<String, ExcelStrcutureImpl>();
	private File templateDirectory;

	public ExcelStuctureFactory() {
	}
	
	@PostConstruct
	private void init() {
		templateDirectory = new File(getClass().getClassLoader().getResource(config.getJsonTemplateDirectory()).getFile());
	}

	public void registerAll() {
		for (File f : templateDirectory.listFiles()) {
			registerByFile(f);
		}
	}

	public ExcelStrcutureImpl getByName(String name) {
		if(name == null){
			return null;
		}
		name = name.toLowerCase();
		if (registeredTemplates.containsKey(name)) {
			return registeredTemplates.get(name);
		}
		if (register(name) == true) {
			logger.info("ExcelStructureFactory: successfully registered '"+name+"'");
			return registeredTemplates.get(name);
		}
		return null;
	}

	public boolean register(String name) {
		String path = templateDirectory.getPath() + File.separator + name;
		File f = new File(path);
		return registerByFile(f);
	}
	
	public boolean isStructureRegistered(String name){
		if(name==null || name.length()==0) return false;
		return registeredTemplates.containsKey(name.toLowerCase());
	}

	protected boolean registerByFile(File f) {
		
		if(f==null){
			return false;
		}
		
		if (!f.exists()) {
			return false;
		}

		try {
			String text = new String(Files.readAllBytes(Paths.get(f.getPath())), StandardCharsets.UTF_8);
			// parseJson
			Gson gson = new Gson();
			ExcelStrcutureImpl parsed = gson.fromJson(text, ExcelStrcutureImpl.class);
			registeredTemplates.put(f.getName().toLowerCase(), parsed);
			
		} catch (IOException e) {
			
			logger.error("error: failed to register file '"+ f.getPath()+ "' in ExcelStrcutureFactory");
			logger.debug(e.getMessage());
			return false;
		}

		return true;
	}
}
