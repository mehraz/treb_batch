package com.treb.treb_batch.io.excel.entity;

import java.util.ArrayList;

public class ExcelStrcutureImpl implements ExcelStructure {
	private ArrayList<SheetStructureImpl> sheets;
	private String name;
	

	public ArrayList<SheetStructureImpl> getSheets() {
		return sheets;
	}

	public void setSheets(ArrayList<SheetStructureImpl> sheets) {
		this.sheets = sheets;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExcelStrcutureImpl() {
		sheets = new ArrayList<SheetStructureImpl>();
	}
	
}
