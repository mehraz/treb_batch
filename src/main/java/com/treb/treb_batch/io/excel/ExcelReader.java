package com.treb.treb_batch.io.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.treb.treb_batch.config.TrebConfig;
import com.treb.treb_batch.io.excel.entity.ExcelStrcutureImpl;
import com.treb.treb_batch.io.excel.entity.ExcelStuctureFactory;
import com.treb.treb_batch.io.excel.entity.SheetStructureImpl;
import com.treb.treb_batch.io.excel.entity.TableBoundaryImpl;
import com.treb.treb_batch.io.map.TableEntityMapFactory;
import com.treb.treb_batch.io.map.TableToEntityMap;

@Configuration
@Scope("prototype")
public class ExcelReader {

	private static final Logger logger = LoggerFactory.getLogger(ExcelReader.class);

	@Autowired
	TrebConfig config;

	@Autowired
	ExcelStuctureFactory excelTemplateFactory;

	@Autowired
	TableEntityMapFactory mapFactory;

	public ExcelReader() {

	}

	public void read(File f) {
		String templateName = getTemplateByName(f.getName());
		logger.info("template:" + templateName + "[path:" + f.getPath() + "]");

		ExcelStrcutureImpl template = excelTemplateFactory.getByName(templateName);
		if (template == null) {
			logger.error("no template found!!");
			return;
		}

		ExcelTableReader reader = new ExcelTableReader(f.getPath());

		for (int i = 0; i < template.getSheets().size(); i++) {
			SheetStructureImpl sheet = (SheetStructureImpl) template.getSheets().get(i);
			logger.info("start reading sheet index '{}', name '{}' , num '{}'", i , sheet.getName(), sheet.getSheetNum() );
			for (TableBoundaryImpl boundry : sheet.getTableBounderies()) {
				try {
					ArrayList<ArrayList<String>> tableData = reader.startReading(sheet.getSheetNum(), boundry);

					@SuppressWarnings("rawtypes")
					TableToEntityMap<?> map = mapFactory.getMapper(boundry.getName());
					if(map==null){
						logger.error("map is null for boundry '"+boundry.getName()+"'");
						continue;
					}
					// hack: adding the date from file name to the data
					String date = getDateFromFileName(f.getName());
					if (tableData.size() > 0 && tableData.get(0).size() > 0) {
						tableData.get(0).set(0, date);
					}
					// save to DB
					int totalInserted = map.streamMapToDB(tableData);
					logger.info("Total Insertion for sheet [sheetName:" + sheet.getName() + ",boundryName:"
							+ boundry.getName() + "] is:" + totalInserted);

				} catch (FileNotFoundException e) {

					logger.error("Read FileNotFoundException" + e.getMessage());
					e.printStackTrace();

				} catch (IOException e) {

					logger.error("Read IOException" + e.getMessage());
					e.printStackTrace();
				}
			}
		}

	}

	private String getDateFromFileName(String name) {
		Pattern pattern = Pattern.compile(config.getExcelDateRegex());
		Matcher matcher = pattern.matcher(name);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return "";
	}

	private String getTemplateByName(String name) {
		Pattern pattern = Pattern.compile(config.getJsonTemplateRegex());
		Matcher matcher = pattern.matcher(name);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return "";
	}
}
