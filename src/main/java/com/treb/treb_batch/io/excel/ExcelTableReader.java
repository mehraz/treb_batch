package com.treb.treb_batch.io.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.treb.treb_batch.io.excel.entity.TableBoundaryImpl;

public class ExcelTableReader {
	
	private Workbook workbook;
	
	public ExcelTableReader(String filePath){
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<ArrayList<String>> startReading(int sheetNum, TableBoundaryImpl boundry) throws FileNotFoundException, IOException{

		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		if(workbook == null){
			return null;
		}
		
		int numberOfSheets = workbook.getNumberOfSheets();
		if(sheetNum> numberOfSheets) {
			return null;
		}
		Sheet sheet = workbook.getSheetAt(sheetNum);
		
		
		Iterator<Row> rowIterator = sheet.iterator();
		int rowCnt=0, colCnt=0;
		
		while(rowIterator.hasNext() && rowCnt<boundry.getEndRow()){
			
			Row row = rowIterator.next();
			if(rowCnt>=boundry.getStartRow() && rowCnt<boundry.getEndRow()){
				
				result.add(new ArrayList<String>());
				Iterator<Cell> cellIterator = row.cellIterator();
				colCnt = 0;
				while(cellIterator.hasNext() && colCnt<boundry.getEndCol()){
					
					Cell cell = cellIterator.next();
					if(colCnt>=boundry.getStartCol() && colCnt<boundry.getEndCol()){
						result.get(result.size()-1).add(cellToString(cell));
					}
					colCnt++;
				}
			}
			rowCnt++;
		}
		return result;
	}
	private String cellToString(Cell cell){
		String val = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			val = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_BLANK:
			val = "";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			val = ((Boolean)cell.getBooleanCellValue()).toString();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			val = Double.toString(cell.getNumericCellValue());
			break;

		default:
			break;
		}
		return val;
	
	}
}
