package com.treb.treb_batch.schedule;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.treb.treb_batch.config.TrebConfig;
import com.treb.treb_batch.io.excel.ExcelReader;


@Component
public class RunScheduler {
	
	private static final Logger logger = LoggerFactory.getLogger(RunScheduler.class);
  //@Autowired
  //private JobLauncher jobLauncher;
  //@Autowired
  //private Job job2;
	
	@Autowired
	ExcelReader reader;
	
	@Autowired
	TrebConfig config;

  public void run() {

    try {
    	
    	logger.info("----------------------------------------------------------------------------------------");
    	logger.info("----------------------- START CHECKING TREB EXCEL FOLDER -------------------------------");
    	//if input is true, it processes One file and then stops.
    	checkReadFolder(false);
    	logger.info("----------------------------------------------------------------------------------------");
    	logger.info("---------------------- FINISHED CHECKING TREB EXCEL FOLDER -----------------------------");

    } catch (Exception e) {
    	e.printStackTrace();
    }

  }
  
  public void checkReadFolder(boolean processOnlyOne){
	  
	  File readFolder = new File(config.getTrebExcelReadFolder());
	  
	  if(!readFolder.exists() || !readFolder.isDirectory()){
		  logger.error("the readFolder doees not exist: '" + readFolder.getPath() +"'");
		  return;
	  }
	  for(File f :readFolder.listFiles()){
		  if(f.isDirectory()){
			  continue;
		  }
		  processFile(f);
		  if(processOnlyOne) break;
	  }
	  
  }
  
  public boolean processFile(File f){
	  if(!readFile(f)){
		  logger.error("failed parsing file '"+f.getPath()+"'");
		  moveFile(f, config.getTrebExcelFailedFolder());
		  return false;
	  }
	  logger.info("Successfully parsed file '"+f.getPath()+"'");
	  return moveFile(f, config.getTrebExcelDoneFolder());
  }
  
  
  public boolean readFile(File f){
	  
	  if(!f.exists() || f.isDirectory()) return false;
	  try{
		  reader.read(f);
		  return true;
	  }catch(Exception e){
		  
		  logger.error("Fatal error: failed to read file '"+f.getPath()+"'. e:"+e.getMessage());
		  e.printStackTrace();
		 return false;
		 
	  }
  }
  
  
  public boolean moveFile(File f, String destPath){
	  
	  try{
		  if(f.renameTo(new File(destPath +"/" + f.getName()))){
			  return true;
		  }
		  return false;
		  
	  } catch(Exception e){
		  return false;
	  }
  }
}