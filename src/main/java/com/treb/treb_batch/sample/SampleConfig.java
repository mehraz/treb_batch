package com.treb.treb_batch.sample;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.dao.MapExecutionContextDao;
import org.springframework.batch.core.repository.dao.MapJobExecutionDao;
import org.springframework.batch.core.repository.dao.MapJobInstanceDao;
import org.springframework.batch.core.repository.dao.MapStepExecutionDao;
import org.springframework.batch.core.repository.support.SimpleJobRepository;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@Configuration
//@PropertySource("classpath:mainProp.properties")
//@EnableBatchProcessing
public class SampleConfig {
	
	@Bean
	public ItemReader<String> reader(){
		ItemReader<String> read = new SimpleRead();
		return read;
	}
	
	@Bean
	public ItemWriter<String> writer(){
		ItemWriter<String> wr = new SimpleWrite();
		return wr;
	}
	
	@Bean
	public SimpleJobRepository jobRepository(){
		SimpleJobRepository jr = new SimpleJobRepository(new MapJobInstanceDao(),new MapJobExecutionDao(),new MapStepExecutionDao(),new MapExecutionContextDao());
		return jr;
	}
	
	@Bean
	public SimpleJobLauncher jobLauncher()
	{
		SimpleJobLauncher jl = new SimpleJobLauncher();
		jl.setJobRepository(jobRepository());
		return jl;
	}
}
