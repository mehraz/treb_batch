package com.treb.treb_batch.sample;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


public class SimpleRead implements ItemReader<String> {
	
	
	
	@Value("${batch.run.max}")
	private int max;
	private int counter = 0;
	

	public SimpleRead(){
		
	}

	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		// TODO Auto-generated method stub
		System.out.println("Reading..."+max);
		counter ++;
		if(counter> max) return null;
		return "yes";
	}
	
}
