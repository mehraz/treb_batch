//To run:
//mongo --port 27017 -u "treb_admin" -p "mhd" --authenticationDatabase "treb" < cleanDB.js

use treb
//clean collections
db.attRowTwnHseRegionTransaction.remove({})
db.coOwnerAptRegionTransaction.remove({})
db.condoAprtRegionTransaction.remove({})
db.condoTwnHseRegionTransaction.remove({})
db.coopAptRegionTransaction.remove({})
db.detachedCondoRegionTransaction.remove({})
db.detachedHseRegionTransaction.remove({})
db.houseRegionTransaction.remove({})
db.house_index_price.remove({})
db.house_type.remove({})
db.housetrans_total.remove({})
db.housetype_price.remove({})
db.linkRegionTransaction.remove({})
db.region.remove({})
db.semidetachedHseRegionTransaction.remove({})

